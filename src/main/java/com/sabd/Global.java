package com.sabd;

public class Global {
    
    public static int NUM_RETRY_REQUEST = 0;
    public static String ZOOKEEPER = "52.18.180.9:2181";
    public static String BROKERS = "localhost:9092";
    public static String ALLUXIO_PATH = "/prova";
    public static String PROGRAM_PATH = "input.csv";
    public static String HDFS_PATH = "hdfs://localhost:9000/prova";
}
