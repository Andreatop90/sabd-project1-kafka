package com.sabd.Workers;

import alluxio.AlluxioURI;
import alluxio.client.file.FileOutStream;
import alluxio.client.file.FileSystem;
import alluxio.exception.AlluxioException;
import com.sabd.Global;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class SendFile implements Runnable {

    private String path;

    public SendFile(String path) {
        this.path = path;
    }

    @Override
    public void run() {


        AlluxioURI path = new AlluxioURI(Global.ALLUXIO_PATH);
        FileSystem fs = FileSystem.Factory.get();


        FileOutStream out = null;
        try {
            String content = new String(Files.readAllBytes(Paths.get(Global.PROGRAM_PATH)));
            //System.out.println(content);
            out = fs.createFile(path);
            out.write(content.getBytes());

            out.close();
        } catch (IOException | AlluxioException e) {
            e.printStackTrace();
        }
    }


}
