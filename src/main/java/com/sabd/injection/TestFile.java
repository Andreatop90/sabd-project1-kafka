package com.sabd.injection;

import alluxio.AlluxioURI;
import alluxio.client.file.FileInStream;
import alluxio.client.file.FileOutStream;
import alluxio.exception.AlluxioException;
import alluxio.exception.FileDoesNotExistException;
import com.sabd.Global;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;

import java.io.IOException;


public class TestFile {

    private String path;

    public TestFile(String path) {
        this.path = path;
    }

    public static void main(String[] args) throws IOException {


        AlluxioURI path = new AlluxioURI(Global.ALLUXIO_PATH);
        alluxio.client.file.FileSystem fs = alluxio.client.file.FileSystem.Factory.get();
        FileOutStream out = null;
        long startTime = System.currentTimeMillis();
        try {
            FileInStream in = fs.openFile(path);

            int content;
            while ((content = in.read()) != -1) {
                //System.out.print((char) content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FileDoesNotExistException e) {
            e.printStackTrace();
        } catch (AlluxioException e) {
            e.printStackTrace();
        }
        long stopTime = System.currentTimeMillis();
        System.out.println("ALLUXIO: " + (stopTime - startTime));

        //HDFS
        startTime = System.currentTimeMillis();
        Configuration conf = new Configuration();
        String filePath = Global.HDFS_PATH;
        Path hpath = new Path(filePath);
        org.apache.hadoop.fs.FileSystem hfs = hpath.getFileSystem(conf);
        FSDataInputStream inputStream = hfs.open(hpath);
        int content;
        while ((content = inputStream.read()) != -1) {
            //System.out.print((char) content);
        }
        // String contentHDFS = new String(Files.readAllBytes(Paths.get(filePath)));
        hfs.close();
        stopTime = System.currentTimeMillis();
        System.out.println("HDFS: " + (stopTime - startTime));
    }

}
